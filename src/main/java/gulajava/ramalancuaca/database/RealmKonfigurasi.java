package gulajava.ramalancuaca.database;

import android.content.Context;

import io.realm.RealmConfiguration;

/**
 * Created by Gulajava Ministudio.
 */
public class RealmKonfigurasi {


    public static RealmConfiguration getKonfigurasiRealm(Context context) {

        return new RealmConfiguration.Builder(context)
                .deleteRealmIfMigrationNeeded()
                .build();
    }
}
