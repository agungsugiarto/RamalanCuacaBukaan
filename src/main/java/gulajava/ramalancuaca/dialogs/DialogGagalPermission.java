package gulajava.ramalancuaca.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.internets.StatusApp;

/**
 * Created by Gulajava Ministudio
 */
public class DialogGagalPermission extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(DialogGagalPermission.this.getActivity());
        builder.setMessage(R.string.keterangan_rasional);
        builder.setNegativeButton(R.string.tomboldialog_keluar, mOnClickListenerBatalkan);

        return builder.create();
    }


    DialogInterface.OnClickListener mOnClickListenerBatalkan = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            StatusApp.getInstance().setStatusAplikasiJalan(false);
            DialogGagalPermission.this.getDialog().dismiss();
            DialogGagalPermission.this.getActivity().finish();
        }
    };
}
