package gulajava.ramalancuaca.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.internets.StatusApp;

/**
 * Created by Gulajava Ministudio
 */
public class DialogGagalLokasi extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(DialogGagalLokasi.this.getActivity());
        builder.setMessage(R.string.keterangan_gps_gagal);
        builder.setPositiveButton(R.string.tomboldialog_setelLokasi, mOnClickListenerSetelanLokasi);
        builder.setNegativeButton(R.string.tomboldialog_batal, mOnClickListenerBatalkan);

        return builder.create();
    }


    DialogInterface.OnClickListener mOnClickListenerSetelanLokasi = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            Intent intentLokasi = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            DialogGagalLokasi.this.startActivity(intentLokasi);
            DialogGagalLokasi.this.getDialog().dismiss();
        }
    };


    DialogInterface.OnClickListener mOnClickListenerBatalkan = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            StatusApp.getInstance().setStatusAplikasiJalan(false);
            DialogGagalLokasi.this.getDialog().dismiss();
            DialogGagalLokasi.this.getActivity().finish();
        }
    };
}
