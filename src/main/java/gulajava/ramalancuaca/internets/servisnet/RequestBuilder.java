package gulajava.ramalancuaca.internets.servisnet;

import gulajava.ramalancuaca.BuildConfig;
import gulajava.ramalancuaca.utilans.Konstan;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by Gulajava Ministudio.
 */
public class RequestBuilder {


    /**
     * POST
     * MINTA DATA CUACA DALAM BEBERAPA HARI
     **/
    //http://api.openweathermap.org/data/2.5/forecast/daily?
    // mode=json&units=metric&cnt=7&APPID=886b8c1b21d5ee6853d45eba4dc6c179&lat=-6.914105&lon=107.601382
    //minta data cuaca dari server dengan menggunakan POST dan lokasi pengguna
    public static Request buildRequestCuacaHarian(String jumlahHari,
                                                  String latitude, String longitudes) {

        String apiKey = BuildConfig.OPEN_WEATHER_MAP_API_KEY;
        String alamatAPI = Konstan.SERVER_CUACA + "/data/2.5/forecast/daily?" + "APPID=" + apiKey;

        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Konstan.PARAM_REQUEST_TIPEDATA, "json");
        formBuilder.add(Konstan.PARAM_REQUEST_SATUAN_UNIT, "metric");
        formBuilder.add(Konstan.PARAM_REQUEST_JUMLAHHARI, jumlahHari);
        formBuilder.add(Konstan.PARAM_REQUEST_LATITUDE, latitude);
        formBuilder.add(Konstan.PARAM_REQUEST_LONGITUDE, longitudes);

        RequestBody requestBodyForm = formBuilder.build();

        return new Request.Builder()
                .url(alamatAPI)
                .post(requestBodyForm)
                .build();
    }


    /**
     * POST
     * MINTA DATA CUACA DALAM SATU HARI, DIBAGI 3 JAM
     **/
    //api.openweathermap.org/data/2.5/forecast?
    //q=Bandung&mode=json&units=metric&cnt=10&APPID=886b8c1b21d5ee6853d45eba4dc6c179
    //http://api.openweathermap.org/data/2.5/forecast?
    // mode=json&units=metric&cnt=7&APPID=886b8c1b21d5ee6853d45eba4dc6c179&lat=-6.914105&lon=107.601382
    public static Request buildRequestCuacaPerJam(String jumlahJam,
                                                  String latitude, String longitudes) {

        String apiKey = BuildConfig.OPEN_WEATHER_MAP_API_KEY;
        String alamatAPI = Konstan.SERVER_CUACA + "/data/2.5/forecast?" + "APPID=" + apiKey;

        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Konstan.PARAM_REQUEST_TIPEDATA, "json");
        formBuilder.add(Konstan.PARAM_REQUEST_SATUAN_UNIT, "metric");
        formBuilder.add(Konstan.PARAM_REQUEST_JUMLAHHARI, jumlahJam);
        formBuilder.add(Konstan.PARAM_REQUEST_LATITUDE, latitude);
        formBuilder.add(Konstan.PARAM_REQUEST_LONGITUDE, longitudes);

        RequestBody requestBodyForm = formBuilder.build();

        return new Request.Builder()
                .url(alamatAPI)
                .post(requestBodyForm)
                .build();
    }


    /**
     * GET
     **/
    //api.openweathermap.org/data/2.5/forecast/daily?
    // q=Bandung&mode=json&units=metric&cnt=7&APPID=886b8c1b21d5ee6853d45eba4dc6c179
    public static Request buildRequestCuacaHarianKota(String jumlahHari,
                                                      String namakota) {

        String apiKey = BuildConfig.OPEN_WEATHER_MAP_API_KEY;
        String alamatAPI = Konstan.SERVER_CUACA + "/data/2.5/forecast/daily?"
                + "APPID=" + apiKey + "&"
                + Konstan.PARAM_REQUEST_TIPEDATA + "=" + "json" + "&"
                + Konstan.PARAM_REQUEST_SATUAN_UNIT + "=" + "metric" + "&"
                + Konstan.PARAM_REQUEST_JUMLAHHARI + "=" + jumlahHari + "&"
                + "q" + "=" + namakota;

        return new Request.Builder()
                .url(alamatAPI)
                .build();
    }

    //api.openweathermap.org/data/2.5/forecast/daily?
    // q=Bandung&mode=json&units=metric&cnt=7&APPID=886b8c1b21d5ee6853d45eba4dc6c179
    public static Request buildRequestCuacaJamKota(String jumlahHari,
                                                   String namakota) {

        String apiKey = BuildConfig.OPEN_WEATHER_MAP_API_KEY;
        String alamatAPI = Konstan.SERVER_CUACA + "/data/2.5/forecast?"
                + "APPID=" + apiKey + "&"
                + Konstan.PARAM_REQUEST_TIPEDATA + "=" + "json" + "&"
                + Konstan.PARAM_REQUEST_SATUAN_UNIT + "=" + "metric" + "&"
                + Konstan.PARAM_REQUEST_JUMLAHHARI + "=" + jumlahHari + "&"
                + "q" + "=" + namakota;

        return new Request.Builder()
                .url(alamatAPI)
                .build();
    }


}
