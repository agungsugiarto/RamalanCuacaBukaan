package gulajava.ramalancuaca.internets.modelnet.cuacaperhari;

import java.util.ArrayList;
import java.util.List;

import gulajava.ramalancuaca.internets.modelnet.cuacaperjam.WeatherItem;

/**
 * Created by Gulajava Ministudio.
 */
public class ForecastHariModel {

    private long dt;

    private TemperaturModel temp;

    private double pressure;
    private double humidity;

    private List<WeatherItem> weather = new ArrayList<>();

    private double speed;
    private double deg;
    private double clouds;
    private double rain;


    public ForecastHariModel() {
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public TemperaturModel getTemp() {
        return temp;
    }

    public void setTemp(TemperaturModel temp) {
        this.temp = temp;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public List<WeatherItem> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherItem> weather) {
        this.weather = weather;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDeg() {
        return deg;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }

    public double getClouds() {
        return clouds;
    }

    public void setClouds(double clouds) {
        this.clouds = clouds;
    }

    public double getRain() {
        return rain;
    }

    public void setRain(double rain) {
        this.rain = rain;
    }
}
